<div class="row">

    <div class="col-md-12">

        <div class="page_headeing" ><h4>Change Password</h4></div>

        <div class="col-sm-10">
            <form action="" method="post" name="form_login" id="form_login">  
                <div class="">
                    <div class="panel-body form-horizontal payment-form">

                        <div class="form-group">
                            <?php if (Session::has('message')) { ?>
                                <?= Session::get('message'); ?>.
                            <?php } ?>
                        </div>

                        <div class="form-group <?= ($errors->has('current_password')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Current Password : </label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="current_password" id="current_password" value="" />
                                <div class="help-block with-errors"><?= ($errors->has('current_password')) ? $errors->first('current_password') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('password')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">New Password : </label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="password" id="password" value="" />
                                <div class="help-block with-errors"><?= ($errors->has('password')) ? $errors->first('password') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('confirm_password')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Confirm Password : </label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password" value="" />
                                <div class="help-block with-errors"><?= ($errors->has('confirm_password')) ? $errors->first('confirm_password') : '' ?></div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="concept" class="col-sm-3 control-label"></label>
                            <div class="col-sm-4">
                                <button type="submit" name="submit" id="submit" value="Save" class="btn btn-sm btn-success">Save</button>

                            </div>
                        </div>


                    </div>
                </div> 
                </fom>
        </div> 
    </div>
</div>