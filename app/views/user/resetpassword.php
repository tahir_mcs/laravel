<div class="container" style="margin: 20% auto;">
    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Reset Password</h3>
                </div>
                <div class="panel-body">
                    <form action="" method="post" name="form_user" id="form_user">
                        <fieldset>
                            <div class="form-group">
                                <?= $message_str ?> <?= Session::get('message'); ?>
                            </div>
                            <?php if ($error == 0) { ?>
                                <div class="form-group <?= ($errors->has('password')) ? 'has-error' : '' ?>">
                                    <input class="form-control" name="password" id="password" value="" placeholder="New Password" type="password" autofocus="">
                                    <div class="help-block with-errors"><?= ($errors->has('password')) ? $errors->first('password') : '' ?></div>
                                </div>
                                <div class="form-group <?= ($errors->has('confirm_password')) ? 'has-error' : '' ?>"">
                                    <input class="form-control"  name="confirm_password" type="password" value="" placeholder="Confirm Password">
                                    <div class="help-block with-errors"><?= ($errors->has('confirm_password')) ? $errors->first('confirm_password') : '' ?></div>
                                </div>
                                <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-sm btn-success">Submit</button>
                                <a href="<?= URL::to('/') ?>"><button type="button" name="cancel" id="cancel" value="Cancel" class="btn btn-sm btn-default">Cancel</button></a>
                            <?php } ?>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
