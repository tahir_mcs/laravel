<div class="container" style="margin: 20% auto;">
    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User Register</h3>
                </div>
                <div class="panel-body">
                    <form action="" method="post" name="form_login" id="form_login">
                        <fieldset>
                            <div class="form-group">
                                <?php if (Session::has('message')) { ?>
                                    <?= Session::get('message'); ?>
                                <?php } ?>
                            </div>
                            <div class="form-group <?= ($errors->has('first_name')) ? 'has-error' : '' ?>">
                                <input class="form-control" name="first_name" id="first_name" value="<?= Input::old("first_name") ?>" type="text" placeholder="First Name" autofocus="">
                                <div class="help-block with-errors"><?= ($errors->has('first_name')) ? $errors->first('first_name') : '' ?></div>
                            </div>

                            <div class="form-group <?= ($errors->has('last_name')) ? 'has-error' : '' ?>">
                                <input class="form-control" name="last_name" id="last_name" value="<?= Input::old("last_name") ?>" type="text" placeholder="Last Name" >
                                <div class="help-block with-errors"><?= ($errors->has('last_name')) ? $errors->first('last_name') : '' ?></div>
                            </div>

                            <div class="form-group <?= ($errors->has('email')) ? 'has-error' : '' ?>">
                                <input class="form-control" name="email" id="email" value="<?= Input::old("email") ?>" type="text" placeholder="Email" >
                                <div class="help-block with-errors"><?= ($errors->has('email')) ? $errors->first('email') : '' ?></div>
                            </div>

                            <div class="form-group <?= ($errors->has('password')) ? 'has-error' : '' ?>"">
                                <input class="form-control"  name="password" type="password" value="" placeholder="Password">
                                <div class="help-block with-errors"><?= ($errors->has('password')) ? $errors->first('password') : '' ?></div>
                            </div>

                            <div class="form-group <?= ($errors->has('confirm_password')) ? 'has-error' : '' ?>"">
                                <input class="form-control"  name="confirm_password" type="password" value="" placeholder="Confirm Password">
                                <div class="help-block with-errors"><?= ($errors->has('confirm_password')) ? $errors->first('confirm_password') : '' ?></div>
                            </div>

                            <div class="form-group <?= ($errors->has('contact')) ? 'has-error' : '' ?>">
                                <input class="form-control phone_us" name="contact" id="contact" value="<?= Input::old("contact") ?>" type="text" placeholder="Contact" >
                                <div class="help-block with-errors"><?= ($errors->has('contact')) ? $errors->first('contact') : '' ?></div>
                            </div>

                            <div class="form-group <?= ($errors->has('date_of_birth')) ? 'has-error' : '' ?>">
                                <div class="input-group">
                                    <input  type="text" name="date_of_birth" id="date_of_birth" value="<?= Input::old("date_of_birth") ?>" type="text" placeholder="Date of Birth" data-date-format="yyyy-mm-dd"  class="date-picker form-control" />
                                    <label for="date_of_birth" class="input-group-addon btn"><i class="fa fa-calendar"></i>

                                    </label>
                                </div>
                                <div class="help-block with-errors"><?= ($errors->has('date_of_birth')) ? $errors->first('date_of_birth') : '' ?></div>
                            </div>



                            <div class="form-group <?= ($errors->has('address')) ? 'has-error' : '' ?>">
                                <textarea name="address" id="address" placeholder="Address" class="form-control"  rows="3" cols="31" ><?= Input::old("address") ?></textarea>
                                <div class="help-block with-errors"><?= ($errors->has('address')) ? $errors->first('address') : '' ?></div>
                            </div>

                            <button type="submit" name="submit" id="submit" value="Register" class="btn btn-sm btn-success">Register</button>
                            <a href="<?= URL::to('/') ?>"><button type="button" name="cancel" id="cancel" value="Cancel" class="btn btn-sm btn-default">Cancel</button></a>
                            <br/>
                            <hr/>
                            <a style="color:#2B2B2B; " href="<?= URL::to('/') ?>">Have an account click here?</a>  
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= HTML::style('public/assets/datepicker/css/datepicker.css') ?>
<?= HTML::script('public/assets/datepicker/js/bootstrap-datepicker.js') ?>
<script>
    $(".date-picker").datepicker();
</script>

