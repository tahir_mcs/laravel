<div class="jumbotron dashboard_custom">
    <h2>Dashboard Area!</h2>      
    <p></p>      
</div>
<?php if (!empty($data)) { ?>
    <div class="col-md-12" style="margin-bottom: 20px;">
        <div class="row">

            <div class="col-lg-12">
                <div class="page_headeing"><h4>Images Gallery</h4></div>
                <?php foreach (array_chunk($data->all(), 4) as $result) { ?>
                    <div class="row"> 
                        <?php foreach ($result as $row) { ?>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="<?= $row->title ?>" data-caption="<?= $row->description ?>" data-image="<?= url() . '/public/assets/images/thumbs/' . $row->image ?>" data-target="#image-gallery">
                                    <?=HTML::image(url().'/public/assets/images/thumbs/'. $row->image, $row->title, array( 'width' => 150, 'height' => 150 ))?>    
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <div class="col-md-12" style="text-align: right;" ><?=$data->links()?></div>
            </div>

            <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="image-gallery-title"></h4>
                        </div>
                        <div class="modal-body">
                            <img id="image-gallery-image" class="img-responsive" src="">
                        </div>
                        <div class="modal-footer">

                            <div class="col-md-2">
                                <button type="button" class="btn btn-primary" id="show-previous-image">Previous</button>
                            </div>

                            <div class="col-md-8 text-justify" id="image-gallery-caption">

                            </div>

                            <div class="col-md-2">
                                <button type="button" id="show-next-image" class="btn btn-default">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <?= HTML::script('public/js/gallery.js') ?>   
    <?php } ?>
