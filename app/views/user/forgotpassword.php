<div class="container" style="margin: 20% auto;">
    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Forgot Password</h3>
                </div>
                <div class="panel-body">
                    <form action="" method="post" name="form_user" id="form_user">
                        <fieldset>
                            <div class="form-group">
                                <?= Session::get('message'); ?>
                            </div>
                            <div class="form-group <?= ($errors->has('email')) ? 'has-error' : '' ?>">
                                <input class="form-control" name="email" id="email" value="<?= Input::old("email") ?>" type="text" placeholder="Email" autofocus="">
                                <div class="help-block with-errors"><?= ($errors->has('email')) ? $errors->first('email') : '' ?></div>
                            </div>

                            <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-sm btn-success">Submit</button>
                            <a href="<?= URL::to('/') ?>"><button type="button" name="cancel" id="cancel" value="Cancel" class="btn btn-sm btn-default">Cancel</button></a>
                            <br/>
                            <hr/>
                            <a style="color:#2B2B2B; " href="<?= URL::to('/user/register') ?>">Register</a>  | <a style="color:#2B2B2B; " href="<?= URL::to('/') ?>">Login</a>    
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
