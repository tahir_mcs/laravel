<div class="row">

    <div class="col-md-12">

        <div class="page_headeing" ><h4>Update Profile</h4></div>

        <div class="col-sm-10">
            <form action="" method="post" name="form_login" id="form_login">  
                <div class="">
                    <div class="panel-body form-horizontal payment-form">

                        <div class="form-group">
                            <?php if (Session::has('message')) { ?>
                                <?= Session::get('message'); ?>.
                            <?php } ?>
                        </div>

                        <div class="form-group <?= ($errors->has('first_name')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">First Name : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="first_name" id="first_name" value="<?= $user['first_name'] ?>" />
                                <div class="help-block with-errors"><?= ($errors->has('first_name')) ? $errors->first('first_name') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('last_name')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Last Name : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="last_name" id="last_name" value="<?= $user['last_name'] ?>" />
                                <div class="help-block with-errors"><?= ($errors->has('last_name')) ? $errors->first('last_name') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="concept" class="col-sm-3 control-label">Email : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="email" id="email" disabled="disabled" value="<?= $user['email'] ?>" />
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('contact')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Contact : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control phone_us" name="contact" id="contact" value="<?= $user['contact'] ?>" />
                                <div class="help-block with-errors"><?= ($errors->has('contact')) ? $errors->first('contact') : '' ?></div>
                            </div>
                        </div>
                        <div class="form-group <?= ($errors->has('date_of_birth')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Date of Birth : </label>
                            <div class="col-sm-5">

                                <div class="input-group">
                                    <input  type="text" name="date_of_birth" id="date_of_birth" value="<?= $user['dob'] ?>" type="text" placeholder="Date of Birth" data-date-format="yyyy-mm-dd"  class="date-picker form-control" />
                                    <label for="date_of_birth" class="input-group-addon btn"><i class="fa fa-calendar"></i>

                                    </label>
                                </div>    

                                <div class="help-block with-errors"><?= ($errors->has('date_of_birth')) ? $errors->first('date_of_birth') : '' ?></div>

                            </div>
                        </div>


                        <div class="form-group <?= ($errors->has('address')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Address : </label>
                            <div class="col-sm-5">
                                <textarea class="form-control" name="address" id="address" rows="3" cols="31" ><?= $user['address'] ?></textarea>
                                <div class="help-block with-errors"><?= ($errors->has('address')) ? $errors->first('address') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="concept" class="col-sm-3 control-label"></label>
                            <div class="col-sm-4">
                                <button type="submit" name="submit" id="submit" value="Save" class="btn btn-sm btn-success">Save</button>

                            </div>
                        </div>


                    </div>
                </div> 
                </fom>
        </div> 
    </div>
</div>
<?= HTML::style('public/assets/datepicker/css/datepicker.css') ?>
<?= HTML::script('public/assets/datepicker/js/bootstrap-datepicker.js') ?>
<script>
    $(".date-picker").datepicker();
</script>