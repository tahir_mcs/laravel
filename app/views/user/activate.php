<div class="container" style="margin: 20% auto;">
    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Account Activation</h3>
                </div>
                <div class="panel-body">
                    <fieldset>
                        <div class="form-group">

                            <div class="alert <?= ($error == 1) ? 'alert-success' : 'alert-danger' ?>" style="padding:8px;">
                                <?= $message_str ?>.
                            </div>

                        </div>
                        <?php if ($error == 1) { ?>
                            <div class="form-group ">
                                <a style="color:#2B2B2B; " href="<?= URL::to('/') ?>">Click here </a> to login
                            </div>
                        <?php } ?>  
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
</div>
