@extends('layouts.master')
@section('content')
{{$content}}
<script >
    $(function () {
        $(".alert").delay(4000).fadeOut('slow');
    });
</script>
@stop

