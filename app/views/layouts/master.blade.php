<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        @section('title')
        <title>{{{$title}}}</title>
        @show

        {{ HTML::style('public/css/bootstrap.min.css') }}
        {{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('public/css/style.css') }}
        {{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}
        {{ HTML::script('public/js/bootstrap.min.js') }}
        {{ HTML::script('public/js/bootbox.js') }}
        {{ HTML::script('public/js/functions.js') }}
        {{ HTML::script('public/js/jquery.mask.min.js') }}
        <script>
            $(document).ready(function () {
                $(".phone_us").mask('(000) 000-0000');
            });
        </script>
    </head>
    <body>

        <div class="container">
            @section('header')
            @show
            @section('breadcrumbs')
            @show
            @yield('content')
            @section('footer')
            @show
        </div>


    </body>
</html>