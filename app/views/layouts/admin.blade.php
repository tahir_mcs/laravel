
@extends('layouts.master')
@section('header')
@include('common.adminheader')
@include('common.admintopnav')
@stop
@section('content')
{{$content}}
@stop
@section('footer')
@include('common.adminfooter')
@stop


