<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Responsive email</title>
        <style type="text/css">
            body {margin: 10px 0; padding: 0 10px; background: #F9F2E7; font-size: 13px;}
            table {border-collapse: collapse;}
            p{ font-size: 14; color:  #141215;}
            a{ color:  #1a3650;}
            td {font-family: arial, sans-serif; color: #333333;}

            @media only screen and (max-width: 480px) {
                body,table,td,p,a,li,blockquote {
                    -webkit-text-size-adjust:none !important;
                }
                table {width: 100% !important;}

                .responsive-image img {
                    height: auto !important;
                    max-width: 100% !important;
                    width: 100% !important;
                }
            }
        </style>
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="640" bgcolor="#FFFFFF">
                        <tr>
                            <td bgcolor="#1a3650" style=" padding: 10px;" height="100" align="left" class="responsive-image">
                                <h2 style="color:#FFFFFF;">Site Logo</h2>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#99f2fe" style="width:100%;" height="8" align="left" class=""></td>
                        </tr>
                        <tr><td style="font-size: 0; line-height: 0;" height="30">&nbsp;</td></tr>
                        <tr>
                            <td style="padding: 10px 10px 20px 10px;">
                                <div style="font-size: 16px;">Hello <?= $name ?>!</div>
                                <br />
                                <hr style="color:#c00;background-color:#c00;height:1px;border:none;" />
                                <div>
                                    <p><?= $msg ?></p>
                                </div>
                            </td>
                        </tr>
                        <tr><td style="font-size: 0; line-height: 0;" height="1" bgcolor="#F9F9F9">&nbsp;</td></tr>
                        <tr><td style="font-size: 0; line-height: 0;" height="30">&nbsp;</td></tr>

                        <tr><td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td></tr>
                        <tr>
                            <td bgcolor="#485465">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
                                    <tr>
                                        <td style="padding: 0 6px; font-size: 12px; color: #FFFFFF;">
                                            Copyright &copy; <?= date('Y') ?>
                                        </td>
                                    </tr>
                                    <tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>