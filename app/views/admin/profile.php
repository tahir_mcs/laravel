<div class="row">

    <div class="col-md-12">

        <div class="page_headeing" ><h4>Profile</h4></div>

        <div class="col-sm-10">
            <form action="" method="post" name="form_login" id="form_login">  
                <div class="">
                    <div class="panel-body form-horizontal payment-form">

                        <div class="form-group">
                            <?php if (Session::has('message')) { ?>
                                <?= Session::get('message'); ?>.
                            <?php } ?>
                        </div>

                        <div class="form-group <?= ($errors->has('name')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Name : </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $data['name'] ?>" />
                                <div class="help-block with-errors"><?= ($errors->has('name')) ? $errors->first('name') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="concept" class="col-sm-3 control-label">Name : </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="email" id="email" disabled="disabled" value="<?= $data['email'] ?>" />
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('password')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Password : </label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="password" id="password" value="" />
                                <div class="help-block with-errors"><?= ($errors->has('password')) ? $errors->first('password') : '' ?></div>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="concept" class="col-sm-3 control-label"></label>
                            <div class="col-sm-4">
                                <button type="submit" name="submit" id="submit" value="Save" class="btn btn-sm btn-success">Save</button>

                            </div>
                        </div>


                    </div>
                </div> 
                </fom>
        </div> 
    </div>
</div>