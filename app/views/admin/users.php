<div class="row">
    <div class="col-md-12">
        <div class="btn-group btn-breadcrumb" style="width: 100%; margin-bottom: 20px;">
            <a href="<?= url() ?>/admin/dashboard" class="btn btn-primary"><i class="fa fa-home"></i>&nbsp</a>
            <a href="#" class="btn btn-primary active" >Users</a>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page_headeing" ><h4>Users Management</h4></div>

        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Contact</th>
                <th>Status</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php
                    foreach ($result_dp as $row) {
                        if ($row['active'] == 1) {
                            $status = '<span  class="label label-success " data-title="Active"  ><i class="fa fa-check"></i> Active</span>';
                        } else {
                            $status = '<span  class="label label-default" data-title="Inctive"  ><i class="fa fa-ban"></i> Inctive</span>';
                        }
                        ?>   
                        <tr>
                            <td><?= $row['first_name'] ?></td>
                            <td><?= $row['last_name'] ?></td>
                            <td><?= $row['email'] ?></td>
                            <td><?= $row['contact'] ?></td>
                            <td id="id_status_<?= $row['id'] ?>" style="padding-top:13px;"><?= $status ?></td>
                            <td><a href="<?= URL::to('/admin/viewuser/' . md5($row['id'])) ?>"><button type="button" class="btn btn-primary btn-xs" data-title="View"  ><i class="fa fa-eye"></i> View</button></a>&nbsp;<a  onclick="delete_record('<?= md5($row['id']) ?>')"  href="javascript:void(0)" ><button type="button" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><i class="fa fa-trash"></i> Delete</button></a></td>
                        </tr>
                    <?php } ?>        
                </tbody>

            </table>
            <div class="col-md-12 pull-right" style="text-align: right;"><?= $result_dp->links() ?></div>

        </div>

    </div>
</div>
<script >
    function delete_record(id) {
        bootbox.confirm('<div class="alert alert-danger alert-error" style="font-size:18px; margin-top: 20px;" >Are you sure you want to delete this ?</div>', function (result) {
            if (result) {
                window.location = "<?= URL::to('/admin/deleteuser') ?>/" + id;
            }
        });
    }
</script>
