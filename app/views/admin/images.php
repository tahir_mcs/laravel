 
<div class="row">
    <div class="col-md-12">
        <div class="btn-group btn-breadcrumb" style="width: 100%; margin-bottom: 20px;">
            <a href="<?= url() ?>/admin/dashboard" class="btn btn-primary"><i class="fa fa-home"></i>&nbsp</a>
            <a href="#" class="btn btn-primary active" >Images</a>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page_headeing" ><h4>Images Management</h4></div>
        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>
                <th>Title</th>
                <th>By</th>
                <th>Image</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($result_dp as $row) { ?>   
                        <tr>
                            <td><?= $row['title'] ?></td>
                            <td><?= $row['first_name'] ?> <?= $row['last_name'] ?></td>
                            <td><a href="<?= URL::to('/admin/viewimage/' . md5($row['image_id'])) ?>"><img class="img-circle" src="<?= url() . '/public/assets/images/thumbs/' . $row['image'] ?>" height="80" width="80" ></a></td>
                            <td><a href="<?= URL::to('/admin/viewimage/' . md5($row['image_id'])) ?>"><button type="button" class="btn btn-primary btn-xs" data-title="View"  ><i class="fa fa-eye"></i> View</button></a> &nbsp; <a onclick="delete_record('<?= md5($row['image_id']) ?>')"  href="javascript:void(0)" ><button type="button" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><i class="fa fa-trash"></i> Delete</button></a></td>
                        </tr>
                    <?php } ?>        
                </tbody>

            </table>
            <div class="col-md-12 pull-right" style="text-align: right;"><?= $result_dp->links() ?></div>
        </div>
    </div>
</div>

<script >
    function delete_record(id) {
        bootbox.confirm('<div class="alert alert-danger alert-error" style="font-size:18px; margin-top: 20px;" >Are you sure you want to delete this ?</div>', function (result) {
            if (result) {
                window.location = "<?= URL::to('/admin/deleteimage') ?>/" + id;
            }
        });
    }
</script>