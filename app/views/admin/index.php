<div class="container" style="margin: 20% auto;">
    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Admin Login</h3>
                </div>
                <div class="panel-body">
                    <form action="<?= URL::to('/admin/adminauth') ?>" method="post" name="form_login" id="form_login">
                        <fieldset>
                            <div class="form-group">
                                <?php if (Session::has('message')) { ?>
                                    <div class="alert alert-danger" style="padding:8px;">
                                        <?= Session::get('message'); ?>.
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group <?= ($errors->has('email')) ? 'has-error' : '' ?>">
                                <input class="form-control" name="email" id="email" value="<?= Input::old("email") ?>" placeholder="Email" type="text" autofocus="">
                                <div class="help-block with-errors"><?= ($errors->has('email')) ? $errors->first('email') : '' ?></div>
                            </div>
                            <div class="form-group <?= ($errors->has('password')) ? 'has-error' : '' ?>"">
                                <input class="form-control"  name="password" type="password" value="" placeholder="Password">
                                <div class="help-block with-errors"><?= ($errors->has('password')) ? $errors->first('password') : '' ?></div>
                            </div>
                            <button type="submit" name="login" id="login" value="login" class="btn btn-sm btn-success">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
