<div class="row">
    <div class="col-md-12">
        <div class="btn-group btn-breadcrumb" style="width: 100%; margin-bottom: 20px;">
            <a href="<?= url() ?>/admin/dashboard" class="btn btn-primary"><i class="fa fa-home"></i>&nbsp</a>
            <a href="<?= url() ?>/admin/users" class="btn btn-primary" >Users</a>
            <a href="#" class="btn btn-primary active" >User Detail</a>

        </div>
    </div>
</div>
<div class="page_headeing" ><h4>User Detail</h4></div>
<div class="resume">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading resume-heading">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-xs-12 col-sm-4">
                                <figure>
                                    <img class="img-circle img-responsive" alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=200" >
                                </figure>



                            </div>

                            <div class="col-xs-12 col-sm-8">
                                <ul class="list-group">
                                    <li class="list-group-item"><i class="fa fa-user"></i> <?= $result_dp['first_name'] ?> <?= $result_dp['last_name'] ?></li>
                                    <li class="list-group-item"><i class="fa fa-envelope"></i> <?= $result_dp['email'] ?></li>
                                    <li class="list-group-item"><i class="fa fa-phone"></i> <?= $result_dp['contact'] ?> </li>
                                    <li class="list-group-item"><i class="fa fa-birthday-cake"></i> <?= $result_dp['dob'] ?></li>
                                    <li class="list-group-item"><i class="fa fa-map-marker"></i> <?= $result_dp['address'] ?></li>


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
