<div class="jumbotron">
    <h2>Welcome to Admin Area!</h2>      
    <p>You  can manage all your modules here</p>      
</div>

<div class="container">
    <div class="row" >
        <div class="col-md-3">
            <div class="list-group">
                <a href="<?= URL::to('admin/users/') ?>" class="list-group-item google-plus">
                    <h3 class="pull-right">
                        <i class="fa fa-user"></i>
                    </h3>
                    <h4 class="list-group-item-heading count">
                        <?= User::get()->count() ?></h4>
                    <p class="list-group-item-text">
                        Users</p>
                </a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="list-group">
                <a href="<?= URL::to('admin/images/') ?>" class="list-group-item tumblr">
                    <h3 class="pull-right">
                        <i class="fa fa-image"></i>
                    </h3>
                    <h4 class="list-group-item-heading count">
                        <?= Image::get()->count() ?></h4>
                    <p class="list-group-item-text">
                        Images</p>
                </a>
            </div>
        </div>

    </div>
</div>

