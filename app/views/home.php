
<div class="">
    <div class="">
        <div class="col-lg-12">
            <h2 class="page-header"><div class="page_headeing"><h4>Images <?= $search_title ?></h4></div></h2>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="col-lg-8">
            <?php if (!$data->isEmpty()) { ?>
                <?php foreach (array_chunk($data->all(), 3) as $result) { ?>
                    <div class="row">
                        <?php foreach ($result as $row) { ?>
                            <div class="col-md-4 portfolio-item">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="<?= $row->title ?>" data-caption="<?= $row->description ?>" data-image="<?= url() . '/public/assets/images/thumbs/' . $row->image ?>" data-target="#image-gallery">
                                    <?= HTML::image(url() . '/public/assets/images/thumbs/' . $row->image, $row->title, array('width' => 150, 'height' => 150)) ?>
                                </a>
                                <h4>
                                    <a class="thumbnail_title" href="#" data-image-id="" data-toggle="modal" data-title="<?= $row->title ?>" data-caption="<?= $row->description ?>" data-image="<?= url() . '/public/assets/images/thumbs/' . $row->image ?>" data-target="#image-gallery"><?= $row->title ?></a>
                                </h4>
                                <p><?= str_limit($row->description, $limit = 50, $end = '...') ?></p>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <hr>
                <div class="col-md-12" style="text-align: center;" ><?= $data->appends(Request::input())->links() ?></div>
                <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="image-gallery-title"></h4>
                            </div>
                            <div class="modal-body" style=" text-align: center;" >
                                <img id="image-gallery-image" class="img-responsive" style="display:inline;" src="">
                            </div>
                            <div class="modal-footer">
                                <div class="col-md-12 text-justify" id="image-gallery-caption">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= HTML::script('public/js/gallery.js') ?> 
            <?php } else { ?>
                <div class="col-md-12" style="text-align: center; padding: 10px; background:  #E7E7E7;" >No Record Found.</div>
            <?php } ?>
        </div>
        <div class="col-lg-4"> 
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-search"></i> Search</h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <form action="" method="get" id="form_search" name="form_search">
                                <div class="form-group ">
                                    <input class="form-control" name="s" id="s" value="" type="text" placeholder="Search" autofocus="">
                                </div>
                                <div class="form-group ">
                                    <div class="input-group">
                                        <input  type="text" name="sd" id="sd" value="" type="text" placeholder="Start Date" data-date-format="yyyy-mm-dd"  class="date-picker form-control" />
                                        <label for="sd" class="input-group-addon btn"><i class="fa fa-calendar"></i>

                                        </label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="input-group">
                                        <input  type="text" name="ed" id="ed" value="" type="text" placeholder="End Date" data-date-format="yyyy-mm-dd"  class="date-picker form-control" />
                                        <label for="ed" class="input-group-addon btn"><i class="fa fa-calendar"></i>

                                        </label>
                                    </div>  
                                </div>
                            </form>

                            <button type="button" name="btn_search" id="btn_search" value="Search" class="btn btn-sm btn-primary">Search</button>

                        </fieldset>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-tags"></i> Tags </h3>
                    </div>
                    <div class="panel-body"><?= Image::getAllTags() ?></div>
                </div>
            </div>
        </div>
        
    </div>
    <?= HTML::style('public/assets/datepicker/css/datepicker.css') ?>
    <?= HTML::script('public/assets/datepicker/js/bootstrap-datepicker.js') ?>
    <script>
        $(".date-picker").datepicker();
        $("#btn_search").click(function () {
            $('#form_search').submit();
        });
    </script>