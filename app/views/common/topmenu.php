<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="<?=in_array('/',Request::segments())?'active':''?>"><a href="<?= URL::to('/') ?>"><i class="fa fa-home"></i> Home</a></li>
                <li class="<?=in_array('dashboard',Request::segments())?'active':''?>"><a href="<?= URL::to('user/dashboard') ?>"> Dashboard</a></li>
                <li class="<?=in_array('images',Request::segments())?'active':''?>" ><a href="<?= URL::to('images/') ?>">Images</a></li>
                <li class="<?=in_array('profile',Request::segments())?'active':''?>"><a href="<?= URL::to('user/profile') ?>">Profile</a></li>
                <li class="<?=in_array('changepassword',Request::segments())?'active':''?>"><a href="<?= URL::to('user/changepassword') ?>">Change Password</a></li>

            </ul>
        </div>
    </div>
</nav>