<div class="head">
    <div class="row-fluid">
        <div class="span12">
            <div class="span12" >
                <h1 class="muted"> <span class="muted_in"> Welcome to Dashboard</span></h1>
            </div>

        </div>
        <div class="span12">

            <div class="span12" style="text-align: right; padding: 8px;">
                Logged in as <a href="<?= URL::to('user/dashboard') ?>"><?= Auth::User()->first_name ?></a>  | <a href="<?= URL::to('user/logout') ?>">Logout </a>
            </div>
        </div>
    </div>
</div> 
