<nav class="navbar navbar-default" style="padding: 5px; text-align: center; min-height: 0px;">
    <div >&COPY; Copyright <?= date('Y') ?></div>
</nav>

<script >
    $(function () {
        $(".alert").delay(4000).fadeOut('slow');
    });
</script>
