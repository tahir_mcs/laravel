<div class="head">
    <div class="row-fluid">
        <div class="span12">
            <div class="span12" >
                <h1 class="muted"> Admin Panel</h1>
            </div>

        </div>
        <div class="span12">

            <div class="span12" style="text-align: right; padding: 8px;">
                Logged in as <a href="<?= URL::to('admin/profile') ?>"><?= Admin::name() ?></a>  | <a href="<?= URL::to('admin/logout') ?>">Logout </a>
            </div>
        </div>
    </div>
</div> 