<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="<?= in_array('dashboard', Request::segments()) ? 'active' : '' ?>"><a href="<?= url() ?>/admin/dashboard"><i class="fa fa-home"></i> Home</a></li>
                <li class="<?= in_array('users', Request::segments()) ? 'active' : '' ?>"><a href="<?= URL::to('admin/users/') ?>">Users</a></li>
                <li class="<?= in_array('images', Request::segments()) ? 'active' : '' ?>"><a href="<?= URL::to('admin/images/') ?>">Images</a></li>

            </ul>
        </div>
    </div>
</nav>
