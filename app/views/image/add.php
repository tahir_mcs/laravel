<link rel="stylesheet" type="text/css" href="<?= url() ?>/public/assets/jquery-crop/css/imgareaselect-animated.css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="<?= url() ?>/public/assets/jquery-crop/js/jquery.imgareaselect.pack.js"></script>
<script type="text/javascript" src="<?= url() ?>/public/assets/jquery-crop/js/script.js"></script>

<div class="row">

    <div class="col-md-12">

        <div class="page_headeing" ><h4>Upload Image</h4></div>

        <div class="col-sm-10">
            <form method="post" name="form_add"   action="" enctype="multipart/form-data"  >
                <input type="hidden" id="x" name="x" value="62" />
                <input type="hidden" id="y" name="y" value="29" />
                <input type="hidden" id="w" name="w" value="138" />
                <input type="hidden" id="h" name="h" value="138" />
                <div class="">
                    <div class="panel-body form-horizontal payment-form">

                        <div class="form-group">
                            <?php if (Session::has('message')) { ?>
                                <?= Session::get('message'); ?>.
                            <?php } ?>
                        </div>

                        <div class="form-group <?= ($errors->has('title')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Title : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="title" id="title" value="<?= Input::old("title") ?>" />
                                <div class="help-block with-errors"><?= ($errors->has('title')) ? $errors->first('title') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('tags')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Tags : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="tags" id="tags" placeholder="" value="<?= Input::old("tags") ?>" />
                                <span style="color:  #999; font-size: 12px;">"," Comma Separated</span>
                                <div class="help-block with-errors"><?= ($errors->has('tags')) ? $errors->first('tags') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="concept" class="col-sm-3 control-label"> </label>
                            <div class="col-sm-5">
                                <img id="uploadPreview" style="display:none;"/>
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('image')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Image : </label>
                            <div class="col-sm-5">
                                <input id="uploadImage" class="" type="file" accept="image/jpeg" name='image' value=""  >
                                <div class="help-block with-errors"><?= ($errors->has('image')) ? $errors->first('image') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('description')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Description : </label>
                            <div class="col-sm-5">
                                <textarea name='description' class="form-control"  cols="31" rows="3" id="description" ><?= Input::old("description") ?></textarea>
                                <div class="help-block with-errors"><?= ($errors->has('description')) ? $errors->first('description') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="concept" class="col-sm-3 control-label"></label>
                            <div class="col-sm-4">
                                <button type="submit" name="submit" id="submit" value="Save" class="btn btn-sm btn-success">Save</button>
                                <a href="<?= URL::to('images/') ?>"><button type="button" name="submit" id="submit" value="Save" class="btn btn-sm btn-default">Cancel</button></a>
        
                            </div>
                        </div>


                    </div>
                </div> 
                </fom>
        </div> 
    </div>
</div>