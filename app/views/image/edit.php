
<div class="row">

    <div class="col-md-12">

        <div class="page_headeing" ><h4>Edit Image Detail</h4></div>

        <div class="col-sm-10">
            <form method="post" name="form_add"   action="" enctype="multipart/form-data"  >
                
                <div class="">
                    <div class="panel-body form-horizontal payment-form">

                        <div class="form-group">
                            <?php if (Session::has('message')) { ?>
                                <?= Session::get('message'); ?>.
                            <?php } ?>
                        </div>

                        <div class="form-group <?= ($errors->has('title')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Title : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="title" id="title" value="<?=$data[0]['title']?>" />
                                <div class="help-block with-errors"><?= ($errors->has('title')) ? $errors->first('title') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group <?= ($errors->has('tags')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Tags : </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="tags" id="tags" placeholder="" value="<?=$data[0]['tags']?>" />
                                <span style="color:  #999; font-size: 12px;">"," Comma Separated</span>
                                <div class="help-block with-errors"><?= ($errors->has('tags')) ? $errors->first('tags') : '' ?></div>
                            </div>
                        </div>

                        

                        <div class="form-group <?= ($errors->has('description')) ? 'has-error' : '' ?>">
                            <label for="concept" class="col-sm-3 control-label">Description : </label>
                            <div class="col-sm-5">
                                <textarea name='description' class="form-control"  cols="31" rows="3" id="description" ><?=$data[0]['description']?></textarea>
                                <div class="help-block with-errors"><?= ($errors->has('description')) ? $errors->first('description') : '' ?></div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="concept" class="col-sm-3 control-label"></label>
                            <div class="col-sm-4">
                                <button type="submit" name="submit" id="submit" value="Save" class="btn btn-sm btn-success">Save</button>
                                 <a href="<?= URL::to('images/') ?>"><button type="button" name="submit" id="submit" value="Save" class="btn btn-sm btn-default">Cancel</button></a>
            
                            </div>
                        </div>


                    </div>
                </div> 
                </fom>
        </div> 
    </div>
</div>