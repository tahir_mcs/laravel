<div class="row">
    <div class="col-md-12">
        <div class="page_headeing" ><h4>Images Management</h4></div>
        <div class="table_headeing" ><a href="<?= URL::to('/images/add') ?>"><button type="button" class="btn btn-sm btn-success"> <i class="fa fa-plus"></i> Add</button></a></div>

        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>
                <th style="width: 35%;">Title</th>
                <th style="width: 25%;">Tags</th>
                <th style="width: 18%;">Image</th>

                <th >Action</th>
                </thead>
                <tbody>
                    <?php
                    foreach ($result_dp as $row) {
                        $tags = explode(',', $row['tags']);
                        $view_tabs = '';
                        foreach ($tags as $tag => $val) {
                            $view_tabs .= "<a class='btn btn-default btn-xs' href='#'> " . $val . "</a> &nbsp";
                        }
                        ?> 

                        <tr>
                            <td><?= $row['title'] ?></td>
                            <td><?= $view_tabs ?></td>
                            <td><a href="<?= URL::to('/images/view/' . md5($row['id'])) ?>"><img class="img-circle" src="<?= url() . '/public/assets/images/thumbs/' . $row['image'] ?>" height="80" width="80" ></a></td>
                            <td><a   href="<?= URL::to('/images/view/' . md5($row['id'])) ?>"><button type="button" class="btn btn-primary btn-xs" data-title="View"  ><i class="fa fa-eye"></i> View</button></a> &nbsp; <a  href="<?= URL::to('/images/edit/' . md5($row['id'])) ?>"><button type="button" class="btn btn-default btn-xs" data-title="Edit"  ><i class="fa fa-pencil"></i> Edit</button></a> &nbsp; <a onclick="delete_record('<?= md5($row['id']) ?>')"  href="javascript:void(0)"><button type="button" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><i class="fa fa-trash"></i> Delete</button></a></td>
                        </tr>
                    <?php } ?>        
                </tbody>

            </table>
            <div class="col-md-12 pull-right" style="text-align: right;"><?= $result_dp->links() ?></div>
        </div>
    </div>
</div>

<script >
    function delete_record(id) {
        bootbox.confirm('<div class="alert alert-danger alert-error" style="font-size:18px; margin-top: 20px;" >Are you sure you want to delete this ?</div>', function (result) {
            if (result) {
                window.location = "<?= URL::to('/images/delete') ?>/" + id;
            }
        });
    }
</script>