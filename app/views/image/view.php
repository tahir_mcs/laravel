<div class="page_headeing" ><h4>Image Detail</h4></div>
<div class="resume">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading resume-heading">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-xs-12 col-sm-4">
                                <figure>
                                    <img class="img-circle img-responsive" alt="" src="<?= url() . '/public/assets/images/thumbs/' . $data[0]['image'] ?>">
                                </figure>
                            </div>
                            <?php
                            $tags = explode(',', $data[0]['tags']);
                            $view_tabs = '';
                            foreach ($tags as $tag => $val) {
                                $view_tabs .= "<a class='btn btn-primary btn-xs' href='#'> " . $val . "</a> &nbsp";
                            }
                            ?>    
                            <div class="col-xs-12 col-sm-8">
                                <ul class="list-group">
                                    <li class="list-group-item"><i class="fa fa-file-text"></i> <?= $data[0]['title'] ?></li>
                                    <li class="list-group-item"><i class="fa fa-tags"></i> <?= $view_tabs ?></li>
                                    <li class="list-group-item" style="text-align: justify;"></i> <?= $data[0]['description'] ?> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>