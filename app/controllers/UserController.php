<?php

class UserController extends BaseController {

    public $layout = 'layouts.dashboard';

    /*
      |--------------------------------------------------------------------------
      | User Login Form
      |--------------------------------------------------------------------------
     */

    public function Index() {
        if (Auth::check()) {
            return Redirect::intended('user/dashboard');
        }
        $this->layout = View::make('layouts.account');
        $this->layout->title = 'User Login';
        $this->layout->content = View::make('user.index');
    }

    /*
      |--------------------------------------------------------------------------
      | User Login Auth
      |--------------------------------------------------------------------------
     */

    public function postUserauth() {
        if (Input::get('login')) {

            $input_data = Input::all();
            $obj_user = new User();
            if ($obj_user->validate_login($input_data)) {
                $email = Input::get('email');
                $password = Input::get('password');

                if (Auth::attempt(array('email' => $email, 'password' => $password, 'active' => 1))) {
                    return Redirect::intended('user/dashboard');
                } else {
                    return Redirect::back()->withInput()->with('message', "Invalid Login information");
                }
            } else {
                $errors = $obj_user->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

    /*
      |--------------------------------------------------------------------------
      | User Logout
      |--------------------------------------------------------------------------
     */

    public function getLogout() {
        Auth::logout();
        return Redirect::intended('user');
    }

    /*
      |--------------------------------------------------------------------------
      | User Register form
      |--------------------------------------------------------------------------
     */

    public function getRegister() {
        if (Auth::check()) {
            return Redirect::intended('user/dashboard');
        }
        $this->layout = View::make('layouts.account');
        $this->layout->title = 'User Register';
        $this->layout->content = View::make('user.register');
    }

    /*
      |--------------------------------------------------------------------------
      | User Registration Process
      |--------------------------------------------------------------------------
     */

    public function postRegister() {
        if (Input::get('submit')) {
            $input_data = Input::all();
            $obj_user = new User();
            if ($obj_user->validate($input_data)) {
                $model_user = new User();
                $model_user->first_name = Input::get('first_name');
                $model_user->last_name = Input::get('last_name');
                $model_user->email = Input::get('email');
                $model_user->password = Hash::make(Input::get('password'));
                $model_user->contact = Input::get('contact');
                $model_user->dob = Input::get('date_of_birth');
                $model_user->address = Input::get('address');
                $model_user->active = 0;
                $model_user->save();

                $data = array('name' => Input::get('first_name'), 'msg' => "You have been successfully registered. <br/>  To activate your account <a href=" . URL::to("/user/activate/" . md5($model_user->id)) . ">Click here</a> ");

                Mail::send('emails.user', $data, function($message) {
                    $message->to(Input::get('email'), Input::get('first_name'))->subject('Welcom');
                });
                return Redirect::back()->with('message', '<div class="alert alert-success" style="padding:8px;">You have been successfully registered. <br/>  Please check your email to activate your account.</div>');
            } else {
                $errors = $obj_user->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

    /*
      |--------------------------------------------------------------------------
      | User Account Activation
      |--------------------------------------------------------------------------
     */

    public function getActivate($id) {
        $message_str = '';
        $error = 0;
        $user = User::whereRaw('md5(id) = ? ', array($id))->get()->toArray();

        if (empty($user)) {
            $message_str = 'Invalid Account Access';
        } else if (!empty($user) && $user[0]['active'] == 1) {
            $message_str = 'Your account is already  activated';
            $error = 1;
        } else {
            $message_str = 'Your account has been successfully activated';
            $error = 1;
            $user_mdl = User::find($user[0]['id']);
            $user_mdl->active = 1;
            $user_mdl->save();
        }
        $this->layout = View::make('layouts.account');
        $this->layout->title = 'Account Activation';
        $this->layout->content = View::make('user.activate', array('message_str' => $message_str, 'error' => $error));
    }

    /*
      |--------------------------------------------------------------------------
      | User Forgot Password
      |--------------------------------------------------------------------------
     */

    public function getForgotpassword() {
        $this->layout = View::make('layouts.account');
        $this->layout->title = 'Forgot Password';
        $this->layout->content = View::make('user.forgotpassword');
    }

    /*
      |--------------------------------------------------------------------------
      | User Forgot Password process
      |--------------------------------------------------------------------------
     */

    public function postForgotpassword() {
        if (Input::get('submit')) {
            $input_data = Input::all();
            $obj_user = new User();
            if ($obj_user->validate_login($input_data)) {
                $user = User::whereRaw('email = ? AND active = 1 ', array(Input::get('email')))->get()->toArray();

                if (empty($user)) {
                    return Redirect::back()->with('message', '<div class="alert alert-danger" style="padding:8px;">Email address not found.</div>');
                } else {
                    $data = array('name' => $user[0]['first_name'], 'msg' => " Thanks for your request to reset password . <br><br> <a href=" . URL::to("/user/resetpassword/" . md5($user[0]['id'])) . ">Click here</a> to Rest Password");
                    Mail::send('emails.user', $data, function($message) use ($user) {
                        $message->to($user[0]['email'], $user[0]['first_name'])->subject('Forgot Password');
                    });
                    return Redirect::back()->with('message', '<div class="alert alert-success" style="padding:8px;">An email message has been sent out with instructions on how to reset your password.</div>');
                }
            } else {
                $errors = $obj_user->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

    /*
      |--------------------------------------------------------------------------
      | User Reset Password.
      |--------------------------------------------------------------------------
     */

    public function getResetpassword($id) {
        $message_str = '';
        $error = 0;
        $user = User::whereRaw('md5(id) = ? ', array($id))->get()->toArray();

        if (empty($user)) {
            $message_str = '<div class="alert alert-danger" style="padding:8px;">Invalid Account Access.</div>';
            $error = 1;
        }
        $this->layout = View::make('layouts.account');
        $this->layout->title = 'Reset Password';
        $this->layout->content = View::make('user.resetpassword', array('message_str' => $message_str, 'error' => $error));
    }

    /*
      |--------------------------------------------------------------------------
      | User Reset Password Process.
      |--------------------------------------------------------------------------
     */

    public function postResetpassword($id) {
        $user = User::whereRaw('md5(id) = ? ', array($id))->get()->toArray();
        if (Input::get('submit')) {
            $input_data = Input::all();
            $obj_user = new User();
            if ($obj_user->validate($input_data)) {
                $model_user = User::find($user[0]['id']);
                $model_user->password = Hash::make(Input::get('password'));
                $model_user->save();
                return Redirect::back()->with('message', '<div class="alert alert-success" style="padding:8px;">Password has been changed successfully. <br/> <a href="' . URL::to("/") . '">Click here to login</a></div>');
            } else {
                $errors = $obj_user->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

    /*
      |--------------------------------------------------------------------------
      | User Profile.
      |--------------------------------------------------------------------------
     */

    public function getProfile() {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }

        $user = User::find(Auth::id());
        $this->layout->title = 'User Profile';
        $this->layout->content = View::make('user.profile', array('user' => $user));
    }

    /*
      |--------------------------------------------------------------------------
      | Update User Profile data
      |--------------------------------------------------------------------------
     */

    public function postProfile() {
        if (Input::get('submit')) {

            $input_data = Input::all();
            $obj_user = new User();
            if ($obj_user->validate($input_data)) {
                $model_user = User::find(Auth::id());
                $model_user->first_name = Input::get('first_name');
                $model_user->last_name = Input::get('last_name');
                $model_user->contact = Input::get('contact');
                $model_user->dob = Input::get('date_of_birth');
                $model_user->address = Input::get('address');
                $model_user->save();
                return Redirect::back()->with('message', '<div class="alert alert-success" style="padding:8px;">Date has been saved successfully</div>');
            } else {
                $errors = $obj_user->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

    /*
      |--------------------------------------------------------------------------
      | User Dashboard.
      |--------------------------------------------------------------------------
     */

    public function getDashboard() {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }
        $data = Image::whereRaw('user_id = ? ', array(Auth::id()))->orderBy('id', 'DESC')->paginate(8);
        $this->layout->title = 'User Dashboard';
        $this->layout->content = View::make('user.dashboard', array('data' => $data));
    }

    /*
      |--------------------------------------------------------------------------
      | User Change Password.
      |--------------------------------------------------------------------------
     */

    public function getChangepassword() {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }
        $this->layout->title = 'Change Password';
        $this->layout->content = View::make('user.changepassword');
    }

    /*
      |--------------------------------------------------------------------------
      | Update User Password.
      |--------------------------------------------------------------------------
     */

    public function postChangepassword() {
        if (Input::get('submit')) {
            $input_data = Input::all();
            $obj_user = new User();
            if ($obj_user->validate($input_data)) {
                $current_password = Input::get('current_password');
                $id = Auth::id();
                $user = User::whereRaw('id = ?  ', array($id))->get()->toArray();
                if (Hash::check($current_password, $user[0]['password'])) {
                    $model_user = User::find(Auth::id());
                    $model_user->password = Hash::make(Input::get('password'));
                    $model_user->save();
                    return Redirect::back()->with('message', '<div class="alert alert-success" style="padding:8px;">Password has been changed successfully</div>');
                } else {
                    return Redirect::back()->with('message', '<div class="alert alert-danger" style="padding:8px;">Your current password entered is not valid.</div>');
                }
            } else {
                $errors = $obj_user->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

}
