<?php

class HomeController extends BaseController {

    public $layout = 'layouts.site';

    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      
     */

    public function index() {
        $search_title = $s_txt = '';
        $q = " deleted_at is NULL";
        if (isset($_GET['tag'])) {
            $search_title = ' - Result for "' . $_GET['tag'] . '" ';
            $data = Image::whereRaw(" FIND_IN_SET('" . $_GET['tag'] . "',tags)  ")->orderBy('id', 'DESC')->paginate(1);
        } else if (isset($_GET['s'])) {
            if ($_GET['s'] != "") {
                $s_txt = ' - Result for "' . $_GET['s'] . '" ';
                $q .= " AND  (title like '%" . $_GET['s'] . "%'  OR description like '%" . $_GET['s'] . "%' OR FIND_IN_SET('" . $_GET['s'] . "',tags) ) ";
            }
            if ($_GET['sd'] != "" && $_GET['ed'] != "") {
                $q .= " AND  DATE(created_at) BETWEEN '" . $_GET['sd'] . "' AND '" . $_GET['ed'] . "' ";
            }
            $search_title = $s_txt;
            $data = Image::whereRaw($q)->orderBy('id', 'DESC')->paginate(6);
        } else {
            $data = Image::orderBy('id', 'DESC')->paginate(6);
        }
        $this->layout->title = 'Laravel Images';
        $this->layout->content = View::make('home', array('data' => $data, 'search_title' => $search_title));
    }

}
