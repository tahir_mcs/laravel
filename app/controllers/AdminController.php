<?php

class AdminController extends BaseController {

    public $layout = 'layouts.admin';

    /*
      |--------------------------------------------------------------------------
      | Admin Login Form
      |--------------------------------------------------------------------------
     */

    public function getIndex() {

        $this->layout = View::make('layouts.account');
        $this->layout->title = 'Admin Login';
        $this->layout->content = View::make('admin.index');
    }

    /*
      |--------------------------------------------------------------------------
      | Admin Login Auth
      |--------------------------------------------------------------------------
     */

    public function postAdminauth() {

        if (Input::get('login')) {

            $input_data = Input::all();
            $obj_admin = new Admin();
            if ($obj_admin->validate($input_data)) {
                $email = Input::get('email');
                $password = Input::get('password');
                $admin_dp = Admin::whereRaw('email = ? AND password = ? ', array($email, md5($password)))->get()->toArray();
                if (!empty($admin_dp)) {
                    Session::put('admin_id', $admin_dp[0]['id']);
                    Session::put('admin_name', $admin_dp[0]['name']);
                    return Redirect::intended('admin/dashboard');
                } else {
                    return Redirect::back()->withInput()->with('message', "Invalid Login information");
                }
            } else {
                $errors = $obj_admin->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }
    
    /*
      |--------------------------------------------------------------------------
      | Profile to change information
      |--------------------------------------------------------------------------
     */

    public function getProfile() {
        
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }
        $data = Admin::find(Admin::id());
        $this->layout->title = 'Profile';
        $this->layout->content = View::make('admin.profile',array('data'=>$data));
    }
    
    /*
      |--------------------------------------------------------------------------
      | Update Profile information
      |--------------------------------------------------------------------------
     */

    public function postProfile() {
        if (Input::get('submit')) {
            $input_data = Input::all();
            $obj_admin = new Admin();
            if ($obj_admin->validate($input_data)) {
                $model = Admin::find(Admin::id());
                $model->name = Input::get('name');
                if(Input::get('password')!=""){
                  $model->password = md5(Input::get('password'));  
                }
                $model->save();
                Session::put('admin_name', Input::get('name'));
                return Redirect::back()->with('message', '<div class="alert alert-success" style="padding:8px;">Date has been saved successfully</div>');
            }else {
                $errors = $obj_admin->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
            
        }
    }

    /*
      |--------------------------------------------------------------------------
      | User Logout
      |--------------------------------------------------------------------------
     */

    public function getLogout() {
        Admin::logout();
        return Redirect::intended('/admin');
    }

    /*
      |--------------------------------------------------------------------------
      | User Dashboard.
      |--------------------------------------------------------------------------
     */

    public function getDashboard() {
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }
        $this->layout->title = 'Admin Dashboard';
        $this->layout->content = View::make('admin.home');
    }

    /*
      |--------------------------------------------------------------------------
      | Manage Users.
      |--------------------------------------------------------------------------
     */

    public function getUsers() {
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }
        $result_dp = User::orderBy('id', 'DESC')->paginate(Config::get('view.record_perpage'));
        $this->layout->title = 'Manage Users';
        $this->layout->content = View::make('admin.users', array('result_dp' => $result_dp));
    }

    /*
      |--------------------------------------------------------------------------
      | View User
      |--------------------------------------------------------------------------
     */

    public function getViewuser($id) {
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }
        $result_dp = User::whereRaw('md5(id) = ? ', array($id))->get()->toArray();
        $this->layout->title = 'User Detail';
        $this->layout->content = View::make('admin.userview', array('result_dp' => $result_dp[0]));
    }

    /*
      |--------------------------------------------------------------------------
      | Delete User
      |--------------------------------------------------------------------------
     */

    public function getDeleteuser($id) {
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }

        User::whereRaw('md5(id) = ? ', array($id))->delete();
        return Redirect::to('/admin/users');
    }

    /*
      |--------------------------------------------------------------------------
      | Manage Images.
      |--------------------------------------------------------------------------
     */

    public function getImages() {
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }
        $result_dp = Image::select('*', 'images.id as image_id', 'users.first_name', 'users.last_name')->join('users', 'images.user_id', '=', 'users.id')->orderBy('images.id', 'DESC')->paginate(Config::get('view.record_perpage'));
        $this->layout->title = 'Images';
        $this->layout->content = View::make('admin.images', array('result_dp' => $result_dp));
    }

    /*
      |--------------------------------------------------------------------------
      | View Image
      |--------------------------------------------------------------------------
     */

    public function getViewimage($id) {
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }
        $data = Image::whereRaw('md5(id) = ?  ', array($id))->get()->toArray();
        $this->layout->title = 'Image Detail';
        $this->layout->content = View::make('admin.imageview', array('data' => $data));
    }

    /*
      |--------------------------------------------------------------------------
      | Delete Image
      |--------------------------------------------------------------------------
     */

    public function getDeleteimage($id) {
        if (!Admin::check()) {
            return Redirect::intended('/admin');
        }

        Image::whereRaw('md5(id) = ? ', array($id))->delete();
        return Redirect::to('/admin/images');
    }

}
