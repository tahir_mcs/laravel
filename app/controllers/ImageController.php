<?php

class ImageController extends BaseController {

    public $layout = 'layouts.dashboard';

    /*
      |--------------------------------------------------------------------------
      | User Images
      |--------------------------------------------------------------------------
     */

    public function getIndex() {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }

        $result_dp = Image::whereRaw('user_id = ? ', array(Auth::id()))->orderBy('id', 'DESC')->paginate(Config::get('view.record_perpage'));
        $this->layout->title = 'Images';
        $this->layout->content = View::make('image.index', array('result_dp' => $result_dp));
    }

    /*
      |--------------------------------------------------------------------------
      | User Add Image form
      |--------------------------------------------------------------------------
     */

    public function getAdd() {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }
        $this->layout->title = 'Add Image';
        $this->layout->content = View::make('image.add');
    }

    /*
      |--------------------------------------------------------------------------
      | User Add Image Process
      |--------------------------------------------------------------------------
     */

    public function postAdd() {
        if (Input::get('submit')) {
            $input_data = Input::all();
            $obj_image = new Image();
            if ($obj_image->validate($input_data)) {
                $file = Input::file('image');
                $ext = $file->getClientOriginalExtension();
                $image_name = uniqid() . '.' . $ext;
                $path_uploads = 'public/assets/images/';
                $path = 'public/assets/images/thumbs/' . $image_name;
                Input::file('image')->move($path_uploads, $image_name);
                $size = getimagesize(public_path() . '/' . $path_uploads . $image_name);
                $nw = $nh = 200; # image with # height 
                $x = (int) $_POST['x'];
                $y = (int) $_POST['y'];
                $w = (int) $_POST['w'] ? $_POST['w'] : $size[0];
                $h = (int) $_POST['h'] ? $_POST['h'] : $size[1];

                $data = file_get_contents(public_path() . '/' . $path_uploads . $image_name);
                $vImg = imagecreatefromstring($data);
                $dstImg = imagecreatetruecolor($nw, $nh);
                imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
                imagejpeg($dstImg, $path);
                imagedestroy($dstImg);

                $model = new Image();
                $model->title = Input::get('title');
                $model->tags = Input::get('tags');
                $model->image = $image_name;
                $model->user_id = Auth::id();
                $model->description = Input::get('description');
                $model->save();

                return Redirect::to('/images');
            } else {
                $errors = $obj_image->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

    /*
      |--------------------------------------------------------------------------
      | View Image
      |--------------------------------------------------------------------------
     */

    public function getView($id) {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }

        $data = Image::whereRaw('md5(id) = ? AND user_id = ? ', array($id, Auth::id()))->get()->toArray();
        $this->layout->title = 'Image Detail';
        $this->layout->content = View::make('image.view', array('data' => $data));
    }

    /*
      |--------------------------------------------------------------------------
      | Delete Record
      |--------------------------------------------------------------------------
     */

    public function getDelete($id) {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }

        Image::whereRaw('md5(id) = ? AND user_id = ? ', array($id, Auth::id()))->delete();
        return Redirect::to('/images');
    }

    /*
      |--------------------------------------------------------------------------
      | Edit Image Detail
      |--------------------------------------------------------------------------
     */

    public function getEdit($id) {
        if (!Auth::check()) {
            return Redirect::intended('user');
        }

        $data = Image::whereRaw('md5(id) = ? AND user_id = ? ', array($id, Auth::id()))->get()->toArray();
        $this->layout->title = 'Edit Image';
        $this->layout->content = View::make('image.edit', array('data' => $data));
    }

    /*
      |--------------------------------------------------------------------------
      | Process Edit Image Detail
      |--------------------------------------------------------------------------
     */

    public function postEdit($id) {
        if (Input::get('submit')) {

            $input_data = Input::all();
            $obj_image = new Image();
            if ($obj_image->validate($input_data)) {
                $data = Image::whereRaw('md5(id) = ? AND user_id = ? ', array($id, Auth::id()))->get()->toArray();
                $model = Image::find($data[0]['id']);
                $model->title = Input::get('title');
                $model->tags = Input::get('tags');
                $model->description = Input::get('description');
                $model->save();
                return Redirect::to('/images');
            } else {
                $errors = $obj_image->errors();
                return Redirect::back()->withInput()->withErrors($errors);
            }
        }
    }

}
