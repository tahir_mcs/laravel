<?php

class CategoryController extends BaseController {

    public $layout = 'layouts.category';

    /**
     * Show the all.
     */
    public function getIndex() {
        $result_dp = Category::orderBy('id', 'DESC')->get();
        $this->layout->title = 'Category Listing';
        $this->layout->content = View::make('category.index', array('result_dp' => $result_dp));
    }

    /**
     * Get the add form.
     */
    public function getAdd() {
        return View::make('category.add');
    }

    /**
     * Save Posted Data.
     */
    public function postAdd() {
        if (Input::get('submit')) {

            $rules = array(
                'name' => 'required|unique:category', // just a normal required validation
            );


            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            } else {
                $model_category = new Category();
                $model_category->name = Input::get('name');
                $model_category->save();
                /*Event::fire('category.test', array(Input::get('name')));
                Event::listen('category.test', function($event) {
                    $data = array('name' => 'tahir', 'msg' => "Category added successfully ");

                    Mail::send('emails.demo', $data, function($message) {
                        $message->to('tahir.hamid@nxb.com.pk', 'tahir')->subject('Welcom');
                    });

                    return true;
                });*/
                return Redirect::to('category');
            }
        }
    }

    /**
     * Get Edit Data.
     */
    public function getEdit($id) {
        $result_dp = Category::find($id);
        return View::make('category.edit', array('result_dp' => $result_dp));
    }

    /**
     * Update Posted Data.
     */
    public function postEdit($id) {

        if (Input::get('submit')) {
            $model_category = Category::find($id);
            $model_category->name = Input::get('name');
            $model_category->save();
            return Redirect::to('category');
        }
    }

    /**
     * Delete Data.
     */
    public function getDelete($id) {
        $result_dp = Category::find($id);
        $result_dp->delete();
        return Redirect::to('category');
    }

}
