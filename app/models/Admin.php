<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Admin extends Eloquent {

    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admins';
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /*
      |--------------------------------------------------------------------------
      | Validation rules.
      |--------------------------------------------------------------------------
     */
    private $rules = array('name' => 'required', 'email' => 'required|email', 'password' => 'sometimes');


    /*
      |--------------------------------------------------------------------------
      | Validation errors will be store in variable .
      |--------------------------------------------------------------------------
     */
    private $errors;

    /*
      |--------------------------------------------------------------------------
      | Validate Input values.
      |--------------------------------------------------------------------------
     */

    public function validate($data) {
        $rules_array = $this->rules;
        foreach ($rules_array as $field => $rule) {
            if (!array_key_exists($field, $data)) {
                unset($rules_array[$field]);
            }
        }
        $v = Validator::make($data, $rules_array);
        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }
        return true;
    }

    /*
      |--------------------------------------------------------------------------
      | Errors input validation.
      |--------------------------------------------------------------------------
     */

    public function errors() {
        return $this->errors;
    }

    /*
      |--------------------------------------------------------------------------
      | function to check admin loggedin
      |--------------------------------------------------------------------------
     */

    public static function check() {
        if (Session::has('admin_id')) {
            return true;
        } else {
            return false;
        }
    }

    /*
      |--------------------------------------------------------------------------
      | function to get admin ID
      |--------------------------------------------------------------------------
     */

    public static function id() {
        if (Session::has('admin_id')) {
            return Session::get('admin_id');
        }
    }

    /*
      |--------------------------------------------------------------------------
      | function to get admin ID
      |--------------------------------------------------------------------------
     */

    public static function name() {
        if (Session::has('admin_name')) {
            return Session::get('admin_name');
        }
    }

    /*
      |--------------------------------------------------------------------------
      | function to remove session data
      |--------------------------------------------------------------------------
     */

    public static function logout() {
        Session::forget('admin_id');
        Session::forget('admin_name');
    }

}
