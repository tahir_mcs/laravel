<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait,
        SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('remember_token');
    /*
      |--------------------------------------------------------------------------
      | soft delete settings .
      |--------------------------------------------------------------------------
     */
    protected $softDelete = true;

    /*
      |--------------------------------------------------------------------------
      | Validation rules.
      |--------------------------------------------------------------------------
     */
   
    private $rules = array('first_name' => 'required', 'last_name' => 'required', 'email' => 'required|email|unique:users',
                'password' => 'required|min:4','current_password' => 'required|min:4', 'confirm_password' => 'required|same:password', 'contact' => 'required', 'date_of_birth' => 'required|date|date_format:"Y-m-d', 'address' => 'required');
    
    private $rules_login = array('email' => 'required|email','password' => 'required|min:4');


    /*
      |--------------------------------------------------------------------------
      | Validation errors will be store in variable .
      |--------------------------------------------------------------------------
     */
    private $errors;

    /*
      |--------------------------------------------------------------------------
      | Validate Input values.
      |--------------------------------------------------------------------------
     */

    public function validate($data) {
        $rules_array = $this->rules;
        foreach ($rules_array as $field => $rule) {
            if (!array_key_exists($field, $data)) {
                unset($rules_array[$field]);
            }
        }
        $v = Validator::make($data, $rules_array);
        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }
        return true;
    }
    /*
      |--------------------------------------------------------------------------
      | Validate Login Form.
      |--------------------------------------------------------------------------
     */

    public function validate_login($data) {
        $rules_array = $this->rules_login;
        foreach ($rules_array as $field => $rule) {
            if (!array_key_exists($field, $data)) {
                unset($rules_array[$field]);
            }
        }
        $v = Validator::make($data, $rules_array);
        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }
        return true;
    }

    /*
      |--------------------------------------------------------------------------
      | Errors input validation.
      |--------------------------------------------------------------------------
     */

    public function errors() {
        return $this->errors;
    }

}
