<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Image extends Eloquent {

    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public function users() {
        return $this->belongsTo('User');
    }

    /*
      |--------------------------------------------------------------------------
      | Validation rules.
      |--------------------------------------------------------------------------
     */

    private $rules = array('title' => 'required', 'tags' => 'required', 'image' => 'required|mimes:jpeg,jpg,png,JPG|max:200', 'description' => 'required');

    /*
      |--------------------------------------------------------------------------
      | Validation errors will be store in variable .
      |--------------------------------------------------------------------------
     */
    private $errors;

    /*
      |--------------------------------------------------------------------------
      | Validate Input values.
      |--------------------------------------------------------------------------
     */

    public function validate($data) {
        $rules_array = $this->rules;
        foreach ($rules_array as $field => $rule) {
            if (!array_key_exists($field, $data)) {
                unset($rules_array[$field]);
            }
        }
        $v = Validator::make($data, $rules_array);
        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }
        return true;
    }

    /*
      |--------------------------------------------------------------------------
      | Errors input validation.
      |--------------------------------------------------------------------------
     */

    public function errors() {
        return $this->errors;
    }

    /*
      |--------------------------------------------------------------------------
      | Get All Tags
      |--------------------------------------------------------------------------
     */

    public static function getAllTags() {
        $tags = '';
        $array_tags = array();
        $rs_dp = Image::get(array('tags'))->toArray();
        if (!empty($rs_dp)) {
            foreach ($rs_dp as $row) {
                $tmp_array_tags = explode(',', $row['tags']);
                foreach ($tmp_array_tags as $key => $val) {
                    $array_tags[] = strtolower($val);
                }
            }
            $array_tags = array_unique($array_tags);
            foreach ($array_tags as $key_tag => $val_tag) {
                $tags .="<a style='margin:3px;' class='btn btn-default btn-xs' href='?tag=" . $val_tag . "'> " . $val_tag . "</a>";
            }
        }

        return $tags;
    }

}
