<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table){
	    		$table->increments('id');
                        $table->string('first_name', 255);
                        $table->string('last_name', 255);
                        $table->string('email', 255);
                        $table->string('password', 255);
                        $table->string('contact', 255);
                        $table->string('dob', 255);
                        $table->text('address');
                        $table->enum('active', array('1', '0'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
