<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
                $this->call('CategoryTableSeeder');
	}

}

class CategoryTableSeeder extends Seeder {
    public function run()
    {
       
       foreach(range(1,3) as $index ){
           Category::create(array('name' => $index.' '.'Category'));
       }
        
           
    }
}
