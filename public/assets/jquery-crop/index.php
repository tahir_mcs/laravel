<!doctype html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<title>jQuery Image Crop - w3bees.com</title>
	<link rel="stylesheet" type="text/css" href="css/imgareaselect-animated.css" />
	<!-- scripts -->
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.imgareaselect.pack.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<style>
	
	
	</style>
</head>
<body>
<div class="wrap">
	<h1><a href="http://www.w3bees.com/2013/08/image-upload-and-crop-with-jquery-and.html">Image Upload and Crop with jQuery and PHP</a></h1>
	<!-- image preview area-->
	<img id="uploadPreview" style="display:none;"/>
	
	<!-- image uploading form -->
	<form action="upload.php" method="post" enctype="multipart/form-data">
		<input id="uploadImage" type="file" accept="image/jpeg" name="image" />
		<input type="submit" value="Upload">

		<!-- hidden inputs -->
		<input type="hidden" id="x" name="x" />
		<input type="hidden" id="y" name="y" />
		<input type="hidden" id="w" name="w" />
		<input type="hidden" id="h" name="h" />
	</form>
	<p>&copy W3bees.com 2013</p>
</div><!--wrap-->
<script type="text/javascript" >
$(document).ready(function () {
  //$('#uploadPreview').imgAreaSelect({ x1: 62, y1: 29, x2: 138, y2: 138 });
});

$(function () {
 $('#uploadPreview').imgAreaSelect({x1: 62, y1: 29, x2: 138, y2: 138, handles: true, parent: "uploadPreview", maxWidth: 150, maxHeight: 150, aspectRatio: '4:3' });
});
</script>


</body>
</html>
